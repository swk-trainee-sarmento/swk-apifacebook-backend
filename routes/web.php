<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get(
    '/',
    function () use ($router) {
        return "Hello";
    }
);

$router->group(['prefix' => 'api'], function() use ($router){
    //Auth

    //api/auth
    $router->group(['prefix' => 'auth'], function() use ($router){
        //api/auth/login
        $router->post('login', 'UsersController@Login');
        //api/auth/register
        $router->post('register', 'UsersController@Regist');
    });

    //Facebook

    //$router->group(['prefix'=>'fb','middleware' => 'auth'], function() use ($router){

    $router->group(['prefix'=>'fb'], function() use ($router){
        $router->get('login', 'FbAccessController@Login');
        $router->get('callback', 'FbAccessController@Callback');
    });




    $router->group(['prefix'=>'fb','middleware' => 'auth'], function() use ($router){


        $router->group(['prefix'=>'user'], function() use ($router){
        $router->post('pages', 'PagesController@UserPages');
        });


        $router->group(['prefix' => 'post'], function() use ($router){
            $router->get('get', 'PostsController@GetPostsFromPage');
            $router->post('schedule', 'PostsController@SchedulePost');
            $router->put('edit/{id}', 'PostsController@EditPostToSend');
            $router->delete('delete', 'PostsController@DeletePostToSend');
        });
    });
});
