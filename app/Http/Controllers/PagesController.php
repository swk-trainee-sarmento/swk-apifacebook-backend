<?php

namespace App\Http\Controllers;

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/autoload.php";

use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpClient\CurlHttpClient as CurlHttpClient;
use App\Models\Pages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class PagesController extends BaseController
{
    public function UserPages()
    {
        $userAuth = Auth::user();
        $userID = $userAuth->id;
        $userFbID = $userAuth->fb_id;

        $user = User::find($userID);
        if(!isset($user))
            return response("User não existe",500);

        $url = 'https://graph.facebook.com/' . $userFbID . '/accounts?';

        $client = new CurlHttpClient();
        $pages = $client->request('GET',$url,[
            'query' => [
                'access_token' => $user->access_token,
            ]
        ]);
        
        $pagesDecode = json_decode($pages->getContent());
        
        foreach($pagesDecode->data as $pages)
        {
            $alreadyExists = Pages::where('fb_id',$pages->id)->where('user_id',$user['id'])->first();
            if(isset($alreadyExists)){
               continue;
            }

            $data = Pages::create([
                'fb_id' => $pages->id,'name'=>$pages->name,'access_token'=> $pages->access_token,'user_id'=>$user['id']
            ]);

            $data->save();
        }
        return response ('Pages Inserted!',201);
    }
    
}


