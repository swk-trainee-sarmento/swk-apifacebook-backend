<?php

namespace App\Http\Controllers;

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/autoload.php";


use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Pages;
use App\Models\Post;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends BaseController
{

    /**
     * Edit schedule post
     *
     * @param [type] $id
     * @param Request $request
     * @return void
     */
    public function EditPostToSend($id,Request $request)
    {
        $user = Auth::user();
        $userID = $user->id;

        $post = Post::find($id);
        $page = Pages::find($post->page_id);
        if(!isset($post))
            return response("Post doesn't exist",500);
        if($page->user_id != $userID)
            return response("User has no permission to edit this post",500);
        if($post->fb_id != NULL)
            return response('Post already sent',500);

        $message = $request->input('message');
        $send_at = $request->input('send_at');
        //$send_at_unix = $send_at->getTimestamp();
        

        $post->update([
            'message' => $message,
            'send_at' => $send_at
        ]);

        return response("Editado com sucesso",200);
    }

    /**
     * Delete schedule post
     *
     * @param [type] $id
     * @return void
     */
    public function DeletePostToSend($id){
        $user = Auth::user();
        $post = Post::find($id);


        if(!isset($post))
            return response('Post não encontrado',500);

        $post->delete();

        return response('Apagado com sucesso',200);
    }

    /**
     * Get posts from a page
     *
     * @param [type] $id
     * @return void
     */
    public function GetPostsFromPage($id)
    {
        $user = Auth::user();
        $posts = Post::where('page_id',$id)->where('fb_id',"<>",NULL)->get();
        if(!isset($posts))
            return response("Pagina não encontrada ou não existem posts enviados à mesma",500);

        foreach($posts as $post){
            echo($post);
        }
    }

    /**
     * Schedule a Post
     *
     * @param Request $request
     * @return void
     */
    public function SchedulePost(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'message' => 'required|string',
            'sent_at' => 'required|Date'
          ]);
  

        $userAuth = Auth::user();
        $userID = $userAuth->id;
        


        $pageID = $request->input('id');
        $message = $request->input('message');

        $page = Pages::find($pageID);
        if(!isset($page))
            return response('Pagina não existe',500);

        if($page['user_id']!=$userID)
            return response("User doesn't have access to this page to post",500);
        
        $sent_at = strtotime($request->input('sent_at'));
        $pageAccessToken = $page['access_token'];

        $post = Post::create([
            'message'=>$message,
            'page_id'=>$pageID,
            'page_access_token'=> $pageAccessToken,
            'send_at' => $sent_at
        ]);
        
        $post->save();

        return response("Post agendado com sucesso",201);
    }

}