<?php

namespace App\Http\Controllers;

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/autoload.php";


use Exception;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends BaseController
{
    
public function Regist(Request $request)
{
  $this->validate($request, [
    'name' => 'required|string',
    'username' => 'required|string',
    'password' => 'required|confirmed',
  ]);

  try {
    $user = new User;
    $user->name = $request->input('name');
    $user->username = $request->input('username');
    $plainPassword = $request->input('password');
    $user->password = Hash::make($plainPassword);

    $user->save();
    return response()->json(['user' => $user, 'message' => 'Criado com sucesso!'], 201);
} catch (Exception $e) {
    return response()->json(['message' => $e->getMessage()], 409);
}
}


public function Login(Request $request)
{
    $this->validate($request, [
      'username' => 'required|string',
      'password' => 'required|string',
    ]);
  
    try{
      //fazer campos individuais

      $username = $request->input('username');
      
      $credentials = $request->only(['username','password']);
      if(!$token = Auth::attempt($credentials)){
           return response()->json(['message' => 'Unauthorized'], 401);
       }
      return response()->json([
      'token'=>$token,
    ],200);
  } catch(Exception $e){
    return response($e->getMessage(),500);
  }
}

}
