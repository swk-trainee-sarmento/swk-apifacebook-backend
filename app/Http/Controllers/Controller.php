<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;


class Controller extends BaseController
{

    protected function getProfile(){
        return Auth::user();
    }

    protected function response($message,$code = 200){
        return response()->json(['message' => $message],$code);
    }

    protected function FbApp(){
        
    }
}
