<?php

namespace App\Http\Controllers;

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/autoload.php";


use Exception;
use Laravel\Lumen\Routing\Controller as BaseController;
use Facebook\Facebook as Facebook;
use Facebook\Exceptions as Exceptions;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
    
class FbAccessController extends BaseController
{
    public function FacebookApp(){
      return (new Facebook([
        'app_id' => env("FB_ID"),
        'app_secret' => env("FB_SECRET"),
        'default_graph_version' => 'v2.10',
      ]));
    }

    public function Login()
    {
        $fb = $this->FacebookApp();
          
          $helper = $fb->getRedirectLoginHelper();
    
          $permissions = [
            'pages_manage_posts',
            'pages_show_list',
            'pages_manage_metadata'
          ];
          $loginUrl = $helper->getLoginUrl("http://localhost/api/fb/callback", $permissions);
          return redirect()->to($loginUrl);
    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function Callback()
    {

      $fb = $this->FacebookApp();
    
      $helper = $fb->getRedirectLoginHelper();
    
      try 
      {
      $accessToken = $helper->getAccessToken();
      }catch(Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    }catch(Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit; 
    }
    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit; 
    }
    
    // The OAuth 2.0 client handler helps us manage access tokens
    $oAuth2Client = $fb->getOAuth2Client();
    
    // Get the access token metadata from /debug_token
    $tokenMetadata = $oAuth2Client->debugToken($accessToken);
    
    // Validation (these will throw FacebookSDKException's when they fail)
    $tokenMetadata->validateAppId(getenv('FB_ID'));

    $tokenMetadata->validateExpiration();
    
    if (! $accessToken->isLongLived()) {
      // Exchanges a short-lived access token for a long-lived one
      try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
      } catch (Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
        exit;
      }
    
    }
    
    $_SESSION['fb_access_token'] = (string) $accessToken;
    
    #id user
    $response = $fb->get('/me?fields=id,name', $accessToken);
    
    
    $userID = $response->getGraphUser()['id'];
    
    $user= User::find("1");
    if(!isset($user))
        return response('User não encontrado!',500);
    

    $user->update([
      "fb_id"=>$userID,
      "access_token"=>$accessToken
    ]);
    return response('FacebookID e access_token associados ao utilizador!',200);
  }
}